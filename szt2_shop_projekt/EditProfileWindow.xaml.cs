﻿// <copyright file="EditProfileWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Szt2_shop_projekt
{
    using System.Windows;
    using DataBaseLibrary;

    /// <summary>
    /// Interaction logic for EditProfileWindoe.xaml
    /// </summary>
    public partial class EditProfileWindow : Window
    {
        private UserViewModel uvm;

        public EditProfileWindow(UserViewModel uvm)
        {
            this.InitializeComponent();
            this.uvm = uvm;
            this.DataContext = this.uvm.LoggedInUser;
            this.nameTextBox.Text = this.uvm.LoggedInUser.USERNAME;
            this.emailTextBox.Text = this.uvm.LoggedInUser.EMAIL;
            this.countryTextBox.Text = this.uvm.LoggedInUser.COUNTRY;
            this.adressTextBox.Text = this.uvm.LoggedInUser.ADRESS;
            this.phoneTextBox.Text = this.uvm.LoggedInUser.PHONE;
        }

        private void OnBackClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void OnSaveClick(object sender, RoutedEventArgs e)
        {
            if (this.emailTextBox.Text == string.Empty || this.countryTextBox.Text == string.Empty || this.adressTextBox.Text == string.Empty || this.phoneTextBox.Text == string.Empty || this.nameTextBox.Text == string.Empty || this.oldPSWTextBox.Password == string.Empty || this.newPSWTextBox.Password == string.Empty)
            {
                MessageBox.Show("Nem maradhat kitöltetlen mező", "Üres mezők", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else if (this.oldPSWTextBox.Password != this.uvm.LoggedInUser.USERPASSSWORD)
            {
                MessageBox.Show("Nem megfelelő a régi jelszó", "Nem jó jelszó", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (this.nameTextBox.Text.Length > USERS.USERNAMELENGTH || this.emailTextBox.Text.Length > USERS.EMAILLENGTH ||
               this.countryTextBox.Text.Length > USERS.COUNTRYLENGTH || this.adressTextBox.Text.Length > USERS.ADRESSLENGTH || this.phoneTextBox.Text.Length > USERS.PHONELENGTH ||
               this.oldPSWTextBox.Password.Length > USERS.USERPASSWORDLENGTH || this.newPSWTextBox.Password.Length > USERS.USERPASSWORDLENGTH)
            {
                MessageBox.Show("Valamelyik adat meghaladta a megengedett hosszúságot", "Túl hosszú adat", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                this.uvm.LoggedInUser.USERNAME = this.nameTextBox.Text;
                this.uvm.LoggedInUser.EMAIL = this.emailTextBox.Text;
                this.uvm.LoggedInUser.COUNTRY = this.countryTextBox.Text;
                this.uvm.LoggedInUser.ADRESS = this.adressTextBox.Text;
                this.uvm.LoggedInUser.PHONE = this.phoneTextBox.Text;
                this.uvm.LoggedInUser.USERPASSSWORD = this.newPSWTextBox.Password;
                this.uvm.UpdateUser(this.uvm.LoggedInUser);
                MessageBox.Show("Sikeresen módosítottuk az adataidat");
                this.DialogResult = false;
            }
        }
    }
}
