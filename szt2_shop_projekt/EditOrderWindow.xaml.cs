﻿// <copyright file="EditOrderWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Szt2_shop_projekt
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using BusinesssLogic;
    using DataBaseLibrary;

    /// <summary>
    /// Interaction logic for EditUsersWindow.xaml
    /// </summary>
    public partial class EditOrderWindow : Window
    {
        private const string Kereso = "Keresés felhasználó ID alapján...";
        private MainLogic ml;
        private OrderViewModel ovm;

        public EditOrderWindow()
        {
            this.InitializeComponent();
            this.ml = MainLogic.Get;
            this.ovm = OrderViewModel.Get;
            this.ovm.AllOrders = this.ml.OrderLogic.GetAll().ToList();
            this.orderListBox.ItemsSource = this.ovm.AllOrders;
            this.keresesTextBox.Text = Kereso;
        }

        private void OnBackClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void OnDeleteClick(object sender, RoutedEventArgs e)
        {
            this.ml.OrderLogic.Delete((ORDERS)this.orderListBox.SelectedItem);
            MessageBox.Show("A megrendelést töröltük", "Törölt megrendelés", MessageBoxButton.OK);
            this.orderListBox.ItemsSource = this.ml.OrderLogic.GetAll();
            this.orderListBox.Items.Refresh();
        }

        private void OnAllOrderButtonClick(object sender, RoutedEventArgs e)
        {
            this.orderListBox.ItemsSource = this.ovm.AllOrders;
        }

        private void OnSearchButton_Click(object sender, RoutedEventArgs e)
        {
            string keresoSzo = this.keresesTextBox.Text;
            List<ORDERS> temp = new List<ORDERS>();
            if (!this.keresesTextBox.Text.All(char.IsDigit))
            {
                MessageBox.Show("A keresőbe csak számok alapján lehet keresni", "Rossz keresés", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                foreach (var item in this.ovm.AllOrders)
                {
                    if (item.UserID == Convert.ToInt32(keresoSzo))
                    {
                        temp.Add(item);
                    }
                }
            }

            this.orderListBox.ItemsSource = temp;
        }

        private void KeresesTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            this.keresesTextBox.Text = Kereso;
        }

        private void KeresesTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            this.keresesTextBox.Text = string.Empty;
        }
    }
}
