﻿// <copyright file="ItemViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Szt2_shop_projekt
{
    using System.Collections.Generic;
    using BusinesssLogic;
    using DataBaseLibrary;

    /// <summary>
    /// Item ViewModel Class
    /// </summary>
    public class ItemViewModel
    {
        private static ItemViewModel vm;
        private MainLogic ml;

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemViewModel"/> class.
        /// Constructor of ItemViewModel class.
        /// </summary>
        private ItemViewModel()
        {
            this.ml = MainLogic.Get;
        }

        /// <summary>
        /// Gets the ItemViewModel.
        /// </summary>
        /// <value>Get ItemViewModel.</value>
        public static ItemViewModel Get
        {
            get
            {
                if (vm == null)
                {
                    vm = new ItemViewModel();
                }

                return vm;
            }
        }

        /// <summary>
        /// Gets or sets the Logged Admin.
        /// </summary>
        /// <value>Get LoggedAdmin.</value>
        public List<ITEMS> AllItems { get; set; }

        /// <summary>
        /// Gets or sets the Logged Admin.
        /// </summary>
        /// <value>Get LoggedAdmin.</value>
        public ITEMS SelectedItem { get; set; }

        /// <summary>
        /// Gets or sets the Logged Admin.
        /// </summary>
        /// <value>Get LoggedAdmin.</value>
        public List<ITEMS> SelectedByCateg { get; set; }

        /// <summary>
        /// Get all item in selected categorie.
        /// </summary>
        /// <param name="categ">Searched categorie.</param>
        /// <returns>ist<ITEMS> </returns>
        public List<ITEMS> CategorieSearch(string categ)
        {
            return this.ml.ItemLogic.SearchByCategorie(this.AllItems, categ);
        }

        /// <summary>
        /// Get item by keyword.
        /// </summary>
        /// <param name="keyword">keyword search parameter.</param>
        /// <returns>List<ITEMS></returns>
        public List<ITEMS> SearchByKeyword(string keyword)
        {
            return this.ml.ItemLogic.SearchByKeyword(this.AllItems, keyword);
        }

        /// <summary>
        /// Get selected CPU.
        /// </summary>
        /// <param name="cpuType">Searched CPU type.</param>
        /// <returns>List<ITEMS></returns>
        public List<ITEMS> CPUCheck(string cpuType)
        {
            return this.ml.ItemLogic.CPUSearch(this.AllItems, cpuType);
        }

        /// <summary>
        /// Get selected HDD.
        /// </summary>
        /// <param name="hddType">Searched HDD type.</param>
        /// <returns>List<ITEMS></returns>
        public List<ITEMS> HDDheck(string hddType)
        {
            return this.ml.ItemLogic.HDDSearch(this.AllItems, hddType);
        }

        /// <summary>
        /// get selected SSD.
        /// </summary>
        /// <param name="ssdType">Searched SSD type.</param>
        /// <returns>List<ITEMS></returns>
        public List<ITEMS> SSDChecked(string ssdType)
        {
            return this.ml.ItemLogic.SSDSearch(this.AllItems, ssdType);
        }

        /// <summary>
        /// Get selected RAM.
        /// </summary>
        /// <param name="ramType">Searched ram type.</param>
        /// <returns>List<ITEMS></returns>
        public List<ITEMS> RAMChecked(string ramType)
        {
            return this.ml.ItemLogic.RAMSearch(this.AllItems, ramType);
        }

        /// <summary>
        /// To Update a slected item.
        /// </summary>
        /// <param name="updatedItem">Updated item parameter.</param>
        public void UpdateItem(ITEMS updatedItem)
        {
            this.ml.ItemLogic.Update(updatedItem);
        }
    }
}
