﻿// <copyright file="ProductEditorWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Szt2_shop_projekt
{
    using System;
    using System.Linq;
    using System.Windows;
    using DataBaseLibrary;

    /// <summary>
    /// Interaction logic for ProductEditorWindow.xaml
    /// </summary>
    public partial class ProductEditorWindow : Window
    {
        private int originalPrice;
        private ITEMS selectedItem;
        private ItemViewModel ivm;

        public ProductEditorWindow(ITEMS selecteditem)
        {
            this.InitializeComponent();
            this.selectedItem = selecteditem;
            this.ivm = ItemViewModel.Get;
            this.DataContext = this.selectedItem;
            this.originalPrice = (int)this.selectedItem.PRICE;
        }

        private void OnBackButtonClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void OnSaveButtonClick(object sender, RoutedEventArgs e)
        {
            if (this.nameTextBox.Text == string.Empty || this.priceTextBox.Text == string.Empty || this.categorieTextBox.Text == string.Empty || this.descriptionTextBox.Text == string.Empty || this.brandTextBox.Text == string.Empty || this.parameterTextBox.Text == string.Empty)
            {
                MessageBox.Show("Nem töltöttél ki egy mezőt", "Hiányzó érték", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (this.nameTextBox.Text.Length > ITEMS.ITEMNAMELENGTH || this.priceTextBox.Text.Length > ITEMS.PRICELENGTH || this.descriptionTextBox.Text.Length > ITEMS.ITEMDESCRIPTIONLENGTH ||
                this.categorieTextBox.Text.Length > ITEMS.ITEMTYPELENGTH || this.parameterTextBox.Text.Length > ITEMS.PARAMETERLENGTH || this.brandTextBox.Text.Length > ITEMS.BRANDLENGTH)
            {
                MessageBox.Show("Valamelyik adat meghaladta a megengedett hosszúságot", "Túl hosszú adat", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (!this.priceTextBox.Text.All(char.IsDigit))
            {
                MessageBox.Show("Az ár mezőnek számot kell tartalmaznia", "Hibás karakter", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                this.selectedItem.ITEMDESCRIPTION = this.descriptionTextBox.Text;
                this.selectedItem.ITEMNAME = this.nameTextBox.Text;
                this.selectedItem.PRICE = Convert.ToDecimal(this.priceTextBox.Text);
                this.selectedItem.ITEMTYPE = this.categorieTextBox.Text;
                this.selectedItem.PARAMETER = this.parameterTextBox.Text;
                this.selectedItem.BRAND = this.brandTextBox.Text;
                this.ivm.UpdateItem(this.selectedItem);
                MessageBox.Show("Adatok sikeresen módosítva");
            }
        }

        private void RadioButtonChecked(object sender, RoutedEventArgs e)
        {
            int price = this.originalPrice;
            if (this.radioButton5.IsChecked == true)
            {
                price = Convert.ToInt32(price * 0.95);
                this.priceTextBox.Text = Convert.ToString(price);
            }
            else if (this.radioButton10.IsChecked == true)
            {
                price = Convert.ToInt32(price * 0.9);
                this.priceTextBox.Text = Convert.ToString(price);
            }
            else if (this.radioButton15.IsChecked == true)
            {
                price = Convert.ToInt32(price * 0.85);
                this.priceTextBox.Text = Convert.ToString(price);
            }
            else if (this.radioButton20.IsChecked == true)
            {
                price = Convert.ToInt32(price * 0.8);
                this.priceTextBox.Text = Convert.ToString(price);
            }
            else if (this.radioButton0.IsChecked == true)
            {
                this.priceTextBox.Text = Convert.ToString(this.originalPrice);
            }
        }
    }
}
