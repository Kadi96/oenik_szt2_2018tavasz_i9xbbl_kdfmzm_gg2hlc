﻿// <copyright file="UserViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Szt2_shop_projekt
{
    using System.Collections.Generic;
    using BusinesssLogic;
    using DataBaseLibrary;

    /// <summary>
    /// UserViewModel Class
    /// </summary>
    public class UserViewModel
    {
        private static UserViewModel uvm;
        private MainLogic ml;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserViewModel"/> class.
        /// Constructor of UserViewModel class.
        /// </summary>
        public UserViewModel()
        {
            this.UserCart = new List<ITEMS>();
        }

        /// <summary>
        /// Gets the UserViewModel.
        /// </summary>
        /// <value>Get UserViewModel.</value>
        public static UserViewModel Get
        {
            get
            {
                if (uvm == null)
                {
                    uvm = new UserViewModel();
                }

                return uvm;
            }
        }

        /// <summary>
        /// Gets or sets the UserCart.
        /// </summary>
        /// <value>Get UserCart.</value>
        public List<ITEMS> UserCart { get; set; }

        /// <summary>
        /// Gets or sets the SelectedUser.
        /// </summary>
        /// <value>Get SelectedUser.</value>
        public USERS SelectedUser { get; set; }

        /// <summary>
        /// Gets or sets the SelectedItem.
        /// </summary>
        /// <value>Get SelectedItem.</value>
        public ITEMS SelectedItem { get; set; }

        /// <summary>
        /// Gets or sets the LoggedInUser.
        /// </summary>
        /// <value>Get LoggedInUser.</value>
        public USERS LoggedInUser { get; set; }

        /// <summary>
        /// Update selected user.
        /// </summary>
        /// <param name="updatedUser">Updated user.</param>
        public void UpdateUser(USERS updatedUser)
        {
            this.ml = MainLogic.Get;
            this.ml.UserLogic.Update(updatedUser);
        }
    }
}
