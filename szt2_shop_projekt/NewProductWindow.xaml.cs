﻿// <copyright file="NewProductWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Szt2_shop_projekt
{
    using System;
    using System.Linq;
    using System.Windows;
    using BusinesssLogic;
    using DataBaseLibrary;

    /// <summary>
    /// Interaction logic for NewProductWindow.xaml
    /// </summary>
    public partial class NewProductWindow : Window
    {
        private MainLogic ml;

        public NewProductWindow()
        {
            this.InitializeComponent();
            this.ml = MainLogic.Get;
        }

        private void OnBackClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void OnSaveButtonClick(object sender, RoutedEventArgs e)
        {
            ITEMS newItem = new ITEMS();
            if (this.nameTextBox.Text == string.Empty || this.priceTextBox.Text == string.Empty || this.categorieTextBox.Text == string.Empty || this.descriptionTextBox.Text == string.Empty || this.parameterTextBox.Text == string.Empty || this.brandTextBox.Text == string.Empty)
            {
                MessageBox.Show("Nem töltöttél ki egy mezőt", "Hiányzó érték", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (!this.priceTextBox.Text.All(char.IsDigit))
            {
                MessageBox.Show("Az ár mezőnek számot kell tertelmeznia", "Hibás karakter", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else if (this.nameTextBox.Text.Length > ITEMS.ITEMNAMELENGTH || this.priceTextBox.Text.Length > ITEMS.PRICELENGTH || this.descriptionTextBox.Text.Length > ITEMS.ITEMDESCRIPTIONLENGTH ||
                  this.categorieTextBox.Text.Length > ITEMS.ITEMTYPELENGTH || this.parameterTextBox.Text.Length > ITEMS.PARAMETERLENGTH || this.brandTextBox.Text.Length > ITEMS.BRANDLENGTH)
            {
                MessageBox.Show("Valamelyik adat meghaladta a megengedett hosszúságot", "Túl hosszú adat", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                newItem.ITEMDESCRIPTION = this.descriptionTextBox.Text;
                newItem.ITEMNAME = this.nameTextBox.Text;
                newItem.PRICE = Convert.ToDecimal(this.priceTextBox.Text);
                newItem.ITEMTYPE = this.categorieTextBox.Text;
                newItem.PARAMETER = this.parameterTextBox.Text;
                newItem.BRAND = this.brandTextBox.Text;
                this.ml.ItemLogic.Create(newItem);
                MessageBox.Show("A termék sikeresen hozzáadva");
                this.DialogResult = false;
            }
        }
    }
}
