﻿// <copyright file="ProfileWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Szt2_shop_projekt
{
    using System.Windows;

    /// <summary>
    /// Interaction logic for ProfileWindow.xaml
    /// </summary>
    public partial class ProfileWindow : Window
    {
        private UserViewModel uvm;

        public ProfileWindow(UserViewModel uvm)
        {
            this.InitializeComponent();
            this.uvm = uvm;
            this.DataContext = this.uvm.LoggedInUser;
        }

        private void OnBackClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void OnEditClick(object sender, RoutedEventArgs e)
        {
            EditProfileWindow editProfileWindow = new EditProfileWindow(this.uvm);
            editProfileWindow.ShowDialog();
            this.DataContext = this.uvm.LoggedInUser;
        }
    }
}
