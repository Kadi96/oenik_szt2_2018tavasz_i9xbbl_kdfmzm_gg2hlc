﻿namespace Szt2_shop_projekt
{
    using System.Windows;
    using BusinesssLogic;
    using DataBaseLibrary;

    /// <summary>
    /// Interaction logic for registrationWindow.xaml
    /// </summary>
    public partial class RegistrationWindow : Window
    {
        MainLogic ML;
        public RegistrationWindow(MainLogic ml)
        {
            this.InitializeComponent();
            this.ML = ml;
        }

        private void OnBackButtonClick(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void OnRegistrationButtonClick(object sender, RoutedEventArgs e)
        {
            foreach (USERS user in this.ML.UserLogic.GetAll())
            {
                if (user.USERNAME.ToLower() == felhasznTxtBox.Text.ToLower())
                {
                    MessageBox.Show("Foglalt felhasználónév", "Foglalt", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            if (this.vezeteknevTxtBox.Text == "" || keresztnevTxtBox.Text == "" || emailTxtBox.Text == "" ||
                orszagTxtBox.Text == "" || lakcimTxtBox.Text == "" || telefonTxtBox.Text == "" ||
                felhasznTxtBox.Text == "" || jelszoTxtBox.Password == "")
            {
                MessageBox.Show("Nem töltött ki egy vagy több mezőt!", "Hiáynzó adatok", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (vezeteknevTxtBox.Text.Length > USERS.LASTNAMELENGTH || keresztnevTxtBox.Text.Length > USERS.FIRSTNAMELENGTH || emailTxtBox.Text.Length > USERS.EMAILLENGTH||
                orszagTxtBox.Text.Length > USERS.COUNTRYLENGTH || lakcimTxtBox.Text.Length > USERS.ADRESSLENGTH || telefonTxtBox.Text.Length > USERS.PHONELENGTH ||
                felhasznTxtBox.Text.Length > USERS.USERNAMELENGTH || jelszoTxtBox.Password.Length > USERS.USERPASSSWORDLENGTH)
            {
                MessageBox.Show("Valamelyik adat meghaladta a megengedett hosszúságot", "Túl hosszú adat", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (jelszoTxtBox.Password != jelszoIsmetlo.Password)
                MessageBox.Show("Nem egyezik a jelszó", "Nem egyező jelszó", MessageBoxButton.OK, MessageBoxImage.Information);
            else
            {
                USERS newUser = new USERS();
                newUser.LASTNAME = vezeteknevTxtBox.Text;
                newUser.FIRSTNAME = keresztnevTxtBox.Text;
                newUser.EMAIL = emailTxtBox.Text;
                newUser.COUNTRY = orszagTxtBox.Text;
                newUser.ADRESS = lakcimTxtBox.Text;
                newUser.PHONE = telefonTxtBox.Text;
                newUser.USERNAME = felhasznTxtBox.Text;
                newUser.USERPASSSWORD = jelszoTxtBox.Password; // ITT HASHELNI
                newUser.PAID = 0;
                this.ML.UserLogic.Create(newUser);
                MessageBox.Show("Sikeres regisztráció", "Sikeres", MessageBoxButton.OK);
                DialogResult = false;
            }
        }
    }
}
