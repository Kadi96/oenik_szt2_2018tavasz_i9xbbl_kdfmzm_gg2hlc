﻿// <copyright file="RegistrationWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Szt2_shop_projekt
{
    using System.Windows;
    using BusinesssLogic;
    using DataBaseLibrary;

    /// <summary>
    /// Interaction logic for registrationWindow.xaml
    /// </summary>
    public partial class RegistrationWindow : Window
    {
        MainLogic ml;

        public RegistrationWindow(MainLogic ml)
        {
            this.InitializeComponent();
            this.ml = ml;
        }

        private void OnBackButtonClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void OnRegistrationButtonClick(object sender, RoutedEventArgs e)
        {
            foreach (USERS user in this.ml.UserLogic.GetAll())
            {
                if (user.USERNAME.ToLower() == this.felhasznTxtBox.Text.ToLower())
                {
                    MessageBox.Show("Foglalt felhasználónév", "Foglalt", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            if (this.vezeteknevTxtBox.Text == string.Empty || this.keresztnevTxtBox.Text == string.Empty || this.emailTxtBox.Text == string.Empty ||
                this.orszagTxtBox.Text == string.Empty || this.lakcimTxtBox.Text == string.Empty || this.telefonTxtBox.Text == string.Empty ||
                this.felhasznTxtBox.Text == string.Empty || this.jelszoTxtBox.Password == string.Empty)
            {
                MessageBox.Show("Nem töltött ki egy vagy több mezőt!", "Hiáynzó adatok", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (this.vezeteknevTxtBox.Text.Length > USERS.LASTNAMELENGTH || this.keresztnevTxtBox.Text.Length > USERS.FIRSTNAMELENGTH || this.emailTxtBox.Text.Length > USERS.EMAILLENGTH||
                this.orszagTxtBox.Text.Length > USERS.COUNTRYLENGTH || this.lakcimTxtBox.Text.Length > USERS.ADRESSLENGTH || this.telefonTxtBox.Text.Length > USERS.PHONELENGTH ||
                this.felhasznTxtBox.Text.Length > USERS.USERNAMELENGTH || this.jelszoTxtBox.Password.Length > USERS.USERPASSWORDLENGTH)
            {
                MessageBox.Show("Valamelyik adat meghaladta a megengedett hosszúságot", "Túl hosszú adat", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (this.jelszoTxtBox.Password != this.jelszoIsmetlo.Password)
            {
                MessageBox.Show("Nem egyezik a jelszó", "Nem egyező jelszó", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                USERS newUser = new USERS();
                newUser.LASTNAME = this.vezeteknevTxtBox.Text;
                newUser.FIRSTNAME = this.keresztnevTxtBox.Text;
                newUser.EMAIL = this.emailTxtBox.Text;
                newUser.COUNTRY = this.orszagTxtBox.Text;
                newUser.ADRESS = this.lakcimTxtBox.Text;
                newUser.PHONE = this.telefonTxtBox.Text;
                newUser.USERNAME = this.felhasznTxtBox.Text;
                newUser.USERPASSSWORD = UsersLogic.Hash(this.jelszoTxtBox.Password);
                newUser.PAID = 0;
                int id = 0;
                foreach (var item in this.ml.UserLogic.GetAll())
                {
                    id++;
                }

                id++;
                newUser.USERID = id;
                this.ml.UserLogic.Create(newUser);
                MessageBox.Show("Sikeres regisztráció", "Sikeres", MessageBoxButton.OK);
                this.DialogResult = false;
            }
        }
    }
}
