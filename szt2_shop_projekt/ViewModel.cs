﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinesssLogic;
using DataBaseLibrary;


namespace szt2_shop_projekt
{
    internal class ViewModel : Bindable
    {
        private static ViewModel VM;
        private Items valsztottTermek;

        // Hogyan kéne elindulni az adatbázissal..
        private ITEMS termek;
        private ITEMS valasztottTermek;

        //


        public Items ValsztottTermek
        {
            get { return valsztottTermek; }
            set { valsztottTermek = value; }
        }

        public static ViewModel Get
        {
            get
            {
                if (VM == null)
                {
                    VM = new ViewModel();
                }
                return VM;
            }
        }



        public ObservableCollection<Items> Termekek { get; set; }

        private ViewModel()
        {
            var BLogics = new BLogic();
            //Termekek = BLogics.Fill();
            

            //Termekek = new ObservableCollection<Items>()
            //{
            //    new Items(){Name="AMD processzor",Categorie="CPU",Price=120000,Description="kutya cica" },
            //    new Items(){Name="500 GB SSD",Price=100,Categorie="SSD",Description="Nagyon jó"},
            //    new Items(){Name="Intel processzor",Categorie="CPU",Price=10000,Description="Elég jó"},
            //    new Items(){Name="XYZ GPU",Categorie="GPU",Price=50000,Description="Nagyon elég jó GPU"},
            //    new Items(){Name="1 TB HDD",Categorie="HDD",Price=200000,Description="Nagyon szuper HDD"},
            //    new Items(){Name="nVidia 1234",Categorie="GPU",Price=180000,Description="A legjobb videókártyaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"},
            //    new Items(){Name="16 GB RAM",Categorie="RAM",Price=80000,Description="Csúcsszuper RAM"},
            //    new Items(){Name="AMD processzor 2.0",Categorie="CPU",Price=150000,Description="2.0 AMD proci közepes teljesítmény" },
            //    new Items(){Name="AMD processzor 3.0",Categorie="CPU",Price=200000,Description="3.0 AMD proci hiper mega teljesítmény" },
            //    new Items(){Name="128 GB SSD",Price=100,Categorie="SSD",Description="Legrosszabb SSD a világon"},
            //    new Items(){Name="1024 GB SSD",Price=100,Categorie="SSD",Description="Legjobb SSD a világon"},
            //};
        }


    }
}
