﻿// <copyright file="AdminViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Szt2_shop_projekt
{
    using DataBaseLibrary;

    /// <summary>
    /// Admin ViewModel Class
    /// </summary>
    internal class AdminViewModel
    {
        private static AdminViewModel avm;

        /// <summary>
        /// Initializes a new instance of the <see cref="AdminViewModel"/> class.
        /// Constructor of AdminViewModel class.
        /// </summary>
        public AdminViewModel()
        {
        }

        /// <summary>
        /// Gets the AdminViewModel.
        /// </summary>
        /// <value>Get AdminViewModel.</value>
        public static AdminViewModel Get
        {
            get
            {
                if (avm == null)
                {
                    avm = new AdminViewModel();
                }

                return avm;
            }
        }

        /// <summary>
        /// Gets or sets the Logged Admin.
        /// </summary>
        /// <value>Get LoggedAdmin.</value>
        public ADMINS LoggedAdmin { get; set; }
    }
}
