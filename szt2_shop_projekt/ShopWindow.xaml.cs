﻿// <copyright file="ShopWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Szt2_shop_projekt
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using BusinesssLogic;
    using DataBaseLibrary;

    /// <summary>
    /// Interaction logic for ProfileWindow.xaml
    /// </summary>
    public partial class ShopWindow : Window
    {
        private const string DefaultText = "Keresés a teljes készletben...";
        private MainLogic ml;
        private ItemViewModel ivm;
        private UserViewModel uvm;
        private List<ITEMS> searchedList;

        public ShopWindow()
        {
            this.InitializeComponent();
            this.keresesTextBox.Text = DefaultText;
            this.ml = MainLogic.Get;
            this.ivm = ItemViewModel.Get;
            this.ivm.AllItems = this.ml.ItemLogic.GetAll().ToList();
            this.uvm = UserViewModel.Get;
            this.DataContext = this.ivm.AllItems;
            this.termekListBox.ItemsSource = this.ivm.AllItems;
            this.searchedList = new List<ITEMS>();
        }

        private void KeresesTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            this.keresesTextBox.Text = string.Empty;
        }

        private void KeresesTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (this.keresesTextBox.Text == string.Empty)
            {
                this.keresesTextBox.Text = DefaultText;
            }
        }

        private void OnExitClick(object sender, RoutedEventArgs e)
        {
            this.uvm.UserCart.Clear();
            this.DialogResult = true;
        }

        private void OnProfileButtonClick(object sender, RoutedEventArgs e)
        {
            ProfileWindow profWindow = new ProfileWindow(this.uvm);
            profWindow.ShowDialog();
        }

        private void OnCartClick(object sender, RoutedEventArgs e)
        {
            CartWindow cartWin = new CartWindow(this.uvm);
            cartWin.ShowDialog();
        }

        private void OnSearchClick(object sender, RoutedEventArgs e)
        {
            string keresoSzo = this.keresesTextBox.Text;
            this.termekListBox.ItemsSource = this.ivm.SearchByKeyword(keresoSzo);
        }

        private void ArSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            List<ITEMS> tempList = new List<ITEMS>();
            foreach (var item in this.searchedList)
            {
                if ((double)item.PRICE <= this.arSlider.Value)
                {
                    tempList.Add(item);
                }
            }

            this.termekListBox.ItemsSource = tempList;
            this.termekListBox.Items.Refresh();
        }

        // CATEGORIE BUTTONS
        private void OnCPUButtonsClick(object sender, RoutedEventArgs e)
        {
            this.termekListBox.ItemsSource = this.ivm.CategorieSearch("CPU");
        }

        private void OnHDDButtonsClick(object sender, RoutedEventArgs e)
        {
            this.termekListBox.ItemsSource = this.ivm.CategorieSearch("HDD");
        }

        private void OnRAMButtonsClick(object sender, RoutedEventArgs e)
        {
            this.termekListBox.ItemsSource = this.ivm.CategorieSearch("Memória");
        }

        private void OnSSDButtonsClick(object sender, RoutedEventArgs e)
        {
            this.termekListBox.ItemsSource = this.ivm.CategorieSearch("SSD");
        }

        private void OnGPUButtonsClick(object sender, RoutedEventArgs e)
        {
            this.termekListBox.ItemsSource = this.ivm.CategorieSearch("GPU");
        }

        private void OnEveryProductButtonClick(object sender, RoutedEventArgs e)
        {
            this.termekListBox.ItemsSource = this.ivm.AllItems;
        }

        // CHECKED CPUS
        private void AMDCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            this.CheckCPU("AMD");
        }

        private void IntelCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            this.CheckCPU("Intel");
        }

        // UNCHECKED CPU
        private void AMDCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            this.UnCheckCPU("AMD");
        }

        private void IntelCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            this.UnCheckCPU("Intel");
        }

        // CHECKED HDD
        private void HDD500CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            this.CheckHDD("500GB");
        }

        private void HDD1TCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            this.CheckHDD("1TB");
        }

        private void HDD2TCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            this.CheckHDD("2TB");
        }

        // UNCHECKED HDD
        private void HDD500CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            this.UnCheckedHDD("500GB");
        }

        private void HDD1TCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            this.UnCheckedHDD("1TB");
        }

        private void HDD2TCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            this.UnCheckedHDD("2TB");
        }

        // CHECKED SSD
        private void SSD128CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            this.CheckSSD("128GB");
        }

        private void SSD256CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            this.CheckSSD("256GB");
        }

        private void SSD512CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            this.CheckSSD("512GB");
        }

        // UNCHECKED SSD
        private void SSD128CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            this.UnCheckedSSD("128GB");
        }

        private void SSD256CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            this.UnCheckedSSD("256GB");
        }

        private void SSD512CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            this.UnCheckedSSD("512GB");
        }

        // CHECKED RAM
        private void RAM6CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            this.CheckRAM("6GB");
        }

        private void RAM8CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            this.CheckRAM("8GB");
        }

        private void RAM16CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            this.CheckRAM("16GB");
        }

        // UNCHECKED RAM
        private void RAM6CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            this.UnCheckedRAM("6GB");
        }

        private void RAM8CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            this.UnCheckedRAM("8GB");
        }

        private void RAM16CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            this.UnCheckedRAM("16GB");
        }

        private void OnToCartButtonClick(object sender, RoutedEventArgs e)
        {
            this.uvm.UserCart.Add((ITEMS)this.termekListBox.SelectedItem);
            MessageBox.Show("Hozzáadva a kosaradhoz", "Kosárba téve", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        // CHECK, UNCHECK "LOGIC"
        private void CheckCPU(string name)
        {
            List<ITEMS> tempList = new List<ITEMS>();
            tempList = this.ivm.CPUCheck(name);
            foreach (var item in tempList)
            {
                this.searchedList.Add(item);
            }

            this.termekListBox.ItemsSource = this.searchedList;
            this.termekListBox.Items.Refresh();
            this.arSlider.Value = 0;
        }

        private void UnCheckCPU(string name)
        {
            List<ITEMS> tempList = new List<ITEMS>();
            tempList = this.ivm.CPUCheck(name);
            foreach (var item in tempList)
            {
                this.searchedList.Remove(item);
            }

            this.termekListBox.ItemsSource = this.searchedList;
            this.termekListBox.Items.Refresh();
            this.arSlider.Value = 0;
        }

        private void CheckHDD(string name)
        {
            List<ITEMS> tempList = new List<ITEMS>();
            tempList = this.ivm.HDDheck(name);
            foreach (var item in tempList)
            {
                this.searchedList.Add(item);
            }

            this.termekListBox.ItemsSource = this.searchedList;
            this.termekListBox.Items.Refresh();
            this.arSlider.Value = 0;
        }

        private void UnCheckedHDD(string name)
        {
            List<ITEMS> tempList = new List<ITEMS>();
            tempList = this.ivm.HDDheck(name);
            foreach (var item in tempList)
            {
                this.searchedList.Remove(item);
            }

            this.termekListBox.ItemsSource = this.searchedList;
            this.termekListBox.Items.Refresh();
            this.arSlider.Value = 0;
        }

        private void CheckSSD(string name)
        {
            List<ITEMS> tempList = new List<ITEMS>();
            tempList = this.ivm.SSDChecked(name);
            foreach (var item in tempList)
            {
                this.searchedList.Add(item);
            }

            this.termekListBox.ItemsSource = this.searchedList;
            this.termekListBox.Items.Refresh();
            this.arSlider.Value = 0;
        }

        private void UnCheckedSSD(string name)
        {
            List<ITEMS> tempList = new List<ITEMS>();
            tempList = this.ivm.SSDChecked(name);
            foreach (var item in tempList)
            {
                this.searchedList.Remove(item);
            }

            this.termekListBox.ItemsSource = this.searchedList;
            this.termekListBox.Items.Refresh();
            this.arSlider.Value = 0;
        }

        private void CheckRAM(string name)
        {
            List<ITEMS> tempList = new List<ITEMS>();
            tempList = this.ivm.RAMChecked(name);
            foreach (var item in tempList)
            {
                this.searchedList.Add(item);
            }

            this.termekListBox.ItemsSource = this.searchedList;
            this.termekListBox.Items.Refresh();
            this.arSlider.Value = 0;
        }

        private void UnCheckedRAM(string name)
        {
            List<ITEMS> tempList = new List<ITEMS>();
            tempList = this.ivm.RAMChecked(name);
            foreach (var item in tempList)
            {
                this.searchedList.Remove(item);
            }

            this.termekListBox.ItemsSource = this.searchedList;
            this.termekListBox.Items.Refresh();
            this.arSlider.Value = 0;
        }
    }
}
