﻿// <copyright file="MainWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Szt2_shop_projekt
{
    using System.Windows;
    using BusinesssLogic;
    using DataBaseLibrary;
    using System.Windows.Threading;
    using System.Threading.Tasks;
    using System;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const string WrongUserName = "Hibás felhasználónév vagy jelszó";

        private MainLogic ml;
        private UserViewModel uvm;
        private AdminViewModel avm;

        public MainWindow()
        {
            this.InitializeComponent();
            this.ml = MainLogic.Get;
            this.ml.AddUserLogic();
            this.ml.AddAdminLogic();
            this.ml.AddItemLogic();
            this.ml.AddOrderLogic();
            this.uvm = UserViewModel.Get;
            this.avm = AdminViewModel.Get;
        }

        private void OnEnterButtonClick(object sender, RoutedEventArgs e)
        {
            USERS loginUser = this.ml.UserLogic.LoggIn(this.felhasznaloTextbox.Text, this.jelszoTextbox.Password);
            if (loginUser != null)
            {
                this.uvm.LoggedInUser = loginUser;
                ShopWindow shopWindow = new ShopWindow();
                this.Hide();
                this.LogginTask(this.uvm.LoggedInUser.USERNAME, this.uvm.LoggedInUser);
                shopWindow.ShowDialog();
                this.felhasznaloTextbox.Clear();
                this.jelszoTextbox.Clear();
                this.wrongUser.Text = string.Empty;
                this.Show();
                Task.WaitAll();
            }
            else if (loginUser == null)
            {
                ADMINS loginAdmin = this.ml.AdminLogic.LoggIn(this.felhasznaloTextbox.Text, this.jelszoTextbox.Password);
                if (loginAdmin != null)
                {
                    this.avm.LoggedAdmin = loginAdmin;
                    AdminWindow adminWindow = new AdminWindow(loginAdmin.USERNAME);
                    this.Hide();
                    adminWindow.ShowDialog();
                    this.felhasznaloTextbox.Clear();
                    this.jelszoTextbox.Clear();
                    this.wrongUser.Text = string.Empty;
                    this.Show();
                }
                else
                {
                    this.wrongUser.Text = WrongUserName;
                    this.felhasznaloTextbox.Clear();
                    this.jelszoTextbox.Clear();
                }
            }
        }

        private void OnRegistrationButtonClick(object sender, RoutedEventArgs e)
        {
            RegistrationWindow rw = new RegistrationWindow(this.ml);
            this.Hide();
            rw.ShowDialog();
            this.Show();
        }

        private void LogginTask(string username, USERS user)
        {
            this.Dispatcher.Invoke(() => { string temp = username;
                Task.Run(() => this.ml.WriteLog($"{temp} : {DateTime.Now.ToString()} ," + "Sikeres belépés"));
            });
        }
    }
}
