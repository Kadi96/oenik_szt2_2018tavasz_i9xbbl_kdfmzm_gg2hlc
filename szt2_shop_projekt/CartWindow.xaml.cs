﻿// <copyright file="CartWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Szt2_shop_projekt
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using BusinesssLogic;
    using DataBaseLibrary;

    /// <summary>
    /// Interaction logic for CartWindow.xaml
    /// </summary>
    public partial class CartWindow : Window
    {
        private UserViewModel uvm;
        private MainLogic ml;

        public CartWindow(UserViewModel uvm)
        {
            this.InitializeComponent();
            this.uvm = uvm;
            this.termekListBox.ItemsSource = this.uvm.UserCart;
            this.DataContext = this.uvm.LoggedInUser;
            this.ml = MainLogic.Get;
            this.Price(this.uvm);
        }

        private void OnDeleteButtonClick(object sender, RoutedEventArgs e)
        {
            this.uvm.UserCart.Remove((ITEMS)this.termekListBox.SelectedItem);
            this.Price(this.uvm);
            this.termekListBox.Items.Refresh();
        }

        private void OnBackButtonClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void OnBuyButtonClick(object sender, RoutedEventArgs e)
        {
            if (this.uvm.UserCart.Count == 0)
            {
                MessageBox.Show("Üres a kosarad", "Nincs termék", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                List<int> tempListID = new List<int>();
                List<int> tempListDB = new List<int>();
                foreach (var itemID in this.uvm.UserCart)
                {
                    if (!tempListID.Contains(itemID.ITEMID))
                    {
                        tempListID.Add(itemID.ITEMID);
                        int counter = 0;
                        int id1 = itemID.ITEMID;
                        foreach (var itemDB in this.uvm.UserCart)
                        {
                            int id2 = itemDB.ITEMID;
                            if (id2 == id1)
                            {
                                counter++;
                            }
                        }

                        tempListDB.Add(counter);
                    }
                }

                for (int i = 0; i < tempListID.Count; i++)
                {
                    ORDERS newOrder = new ORDERS();
                    newOrder.ItemID = tempListID.ToArray()[i];
                    newOrder.UserID = this.uvm.LoggedInUser.USERID;
                    newOrder.NumberOfItem = tempListDB.ToArray()[i];
                    newOrder.OrderDate = DateTime.Now;
                    this.ml.OrderLogic.Create(newOrder);
                }

                MessageBox.Show("A vásárlás sikeres volt. A pénz átutalására 5 nap áll rendelkezésre!", "Sikeres vásárlás", MessageBoxButton.OK);
                this.uvm.UserCart.Clear();
                this.DialogResult = true;
            }
        }

        private void Price(UserViewModel uvm)
        {
            if (uvm.UserCart.Count == 0)
            {
                this.priceLabel.Content = "Összesen: 0 Ft";
            }
            else
            {
                int temp = 0;
                foreach (var item in uvm.UserCart)
                {
                    temp = temp + (int)item.PRICE;
                }

                this.priceLabel.Content = "Összesen: " + Convert.ToString(temp) + " Ft";
            }
        }
    }
}
