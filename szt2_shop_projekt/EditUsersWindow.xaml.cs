﻿// <copyright file="EditUsersWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Szt2_shop_projekt
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using BusinesssLogic;
    using DataBaseLibrary;

    /// <summary>
    /// Interaction logic for EditUsersWindow.xaml
    /// </summary>
    public partial class EditUsersWindow : Window
    {
        private const string Kereso = "Felhasználók kereséséhez írjon ide...";
        private UserViewModel uvm;
        private MainLogic ml;

        public EditUsersWindow()
        {
            this.InitializeComponent();
            this.uvm = UserViewModel.Get;
            this.ml = MainLogic.Get;
            this.keresesTextBox.Text = Kereso;
            this.userListBox.ItemsSource = this.ml.UserLogic.GetAll().ToList();
        }

        private void OnSearchButtonClick(object sender, RoutedEventArgs e)
        {
            List<USERS> tempUsers = new List<USERS>();
            foreach (var item in this.ml.UserLogic.GetAll().ToList())
            {
                if (item.USERNAME.Contains(this.keresesTextBox.Text))
                {
                    tempUsers.Add(item);
                }
            }

            this.userListBox.ItemsSource = tempUsers;
        }

        private void OnBakcButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void OnDataButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.userListBox.SelectedItem == null)
            {
                MessageBox.Show("Nincs kiválaszott felhasználó", string.Empty, MessageBoxButton.OK);
            }
            else
            {
                UserViewWindow userViewWindow = new UserViewWindow((USERS)this.userListBox.SelectedItem);
                userViewWindow.ShowDialog();
            }
        }

        private void OnDeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.userListBox.SelectedItem == null)
            {
                MessageBox.Show("Nincs kiválaszott felhasználó", string.Empty, MessageBoxButton.OK);
            }
            else
            {
                this.ml.UserLogic.Delete((USERS)this.userListBox.SelectedItem);
                MessageBox.Show("A felhasználót töröltük", "Törölt felhasználó", MessageBoxButton.OK);
                this.userListBox.ItemsSource = this.ml.UserLogic.GetAll();
                this.userListBox.Items.Refresh();
            }
        }

        private void KeresesTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            this.keresesTextBox.Text = string.Empty;
        }

        private void KeresesTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (this.keresesTextBox.Text == string.Empty)
            {
                this.keresesTextBox.Text = Kereso;
            }
        }

        private void AllUsersButtonClick(object sender, RoutedEventArgs e)
        {
            this.userListBox.ItemsSource = this.ml.UserLogic.GetAll().ToList();
        }
    }
}
