﻿// <copyright file="AdminWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Szt2_shop_projekt
{
    using System.Linq;
    using System.Windows;
    using BusinesssLogic;
    using DataBaseLibrary;

    /// <summary>
    /// Interaction logic for AdminWindow.xaml
    /// </summary>
    public partial class AdminWindow : Window
    {
        private const string DefaultText = "Kereséshez írjon ide...";
        private ItemViewModel ivm;
        private MainLogic ml;

        public AdminWindow(string adminName)
        {
            this.InitializeComponent();
            this.ivm = ItemViewModel.Get;
            this.ml = MainLogic.Get;
            this.ivm.AllItems = this.ml.ItemLogic.GetAll().ToList();
            this.DataContext = this.ivm.AllItems;
            this.termekListBox.ItemsSource = this.ivm.AllItems;
            this.adminLabel.Content = adminName + " belépve";
            this.keresesTextBox.Text = DefaultText;
        }

        private void OnEditButtonClick(object sender, RoutedEventArgs e)
        {
            if (this.termekListBox.SelectedItem == null)
            {
                MessageBox.Show("Nincs kiválasztott termék");
            }
            else
            {
                ProductEditorWindow editorWindow = new ProductEditorWindow((ITEMS)this.termekListBox.SelectedItem);
                editorWindow.ShowDialog();
            }
        }

        private void OnDeleteButtonClick(object sender, RoutedEventArgs e)
        {
            if (this.termekListBox.SelectedItem == null)
            {
                MessageBox.Show("Nincs kiválaszott termék", string.Empty, MessageBoxButton.OK);
            }
            else
            {
                this.ml.ItemLogic.Delete((ITEMS)this.termekListBox.SelectedItem);
                MessageBox.Show("A terméket töröltük", "Törölt termék", MessageBoxButton.OK);
                this.termekListBox.ItemsSource = this.ml.ItemLogic.GetAll();
                this.termekListBox.Items.Refresh();
            }
        }

        private void OnNewProductButtonClick(object sender, RoutedEventArgs e)
        {
            NewProductWindow newProductWin = new NewProductWindow();
            newProductWin.ShowDialog();
        }

        private void OnExitButtonClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            this.keresesTextBox.Text = string.Empty;
        }

        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (this.keresesTextBox.Text == string.Empty)
            {
                this.keresesTextBox.Text = DefaultText;
            }
        }

        private void OnSearchButtonClick(object sender, RoutedEventArgs e)
        {
            string keresoSzo = this.keresesTextBox.Text;
            this.termekListBox.ItemsSource = this.ivm.SearchByKeyword(keresoSzo);
        }

        private void OnEveryProductButtonClick(object sender, RoutedEventArgs e)
        {
            this.termekListBox.ItemsSource = this.ml.ItemLogic.GetAll().ToList();
        }

        private void OnUsersClick(object sender, RoutedEventArgs e)
        {
            EditOrderWindow editUsersWindow = new EditOrderWindow();
            editUsersWindow.ShowDialog();
        }

        private void OnUsersButtonClick(object sender, RoutedEventArgs e)
        {
            EditUsersWindow editUsersWindow = new EditUsersWindow();
            editUsersWindow.ShowDialog();
        }
    }
}
