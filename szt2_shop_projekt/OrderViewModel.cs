﻿// <copyright file="OrderViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Szt2_shop_projekt
{
    using System.Collections.Generic;
    using DataBaseLibrary;

    /// <summary>
    /// OrderViewModel Class
    /// </summary>
    public class OrderViewModel
    {
        private static OrderViewModel ovm;

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderViewModel"/> class.
        /// Constructor of OrderViewModel class.
        /// </summary>
        public OrderViewModel()
        {
        }

        /// <summary>
        /// Gets the OrderViewModel.
        /// </summary>
        /// <value>Get OrderViewModel.</value>
        public static OrderViewModel Get
        {
            get
            {
                if (ovm == null)
                {
                    ovm = new OrderViewModel();
                }

                return ovm;
            }
        }

        /// <summary>
        /// Gets or sets the SelectedOrder.
        /// </summary>
        /// <value>Get Selected order.</value>
        public ORDERS SelectedOrder { get; set; }

        /// <summary>
        /// Gets or sets the All orders.
        /// </summary>
        /// <value>Get All orders.</value>
        public List<ORDERS> AllOrders { get; set; }
    }
}
