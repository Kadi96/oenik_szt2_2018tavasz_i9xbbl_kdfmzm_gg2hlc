﻿// <copyright file="UserViewWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Szt2_shop_projekt
{
    using System.Windows;
    using DataBaseLibrary;

    /// <summary>
    /// Interaction logic for UserViewWindow.xaml
    /// </summary>
    public partial class UserViewWindow : Window
    {
        public UserViewWindow(USERS selectedUser)
        {
            this.InitializeComponent();
            this.DataContext = selectedUser;
        }

        private void OnBackButtonClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
