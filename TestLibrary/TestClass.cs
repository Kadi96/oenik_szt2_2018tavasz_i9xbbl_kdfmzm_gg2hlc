﻿// <copyright file="TestClass.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace TestLibrary
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using BusinesssLogic;
    using DataBaseLibrary;
    using DataBaseLibrary.Interfaces;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class TestClass
    {
        [Test]
        public void AddUserTest()
        {
            // ARRANGE
            Mock<IRepository<USERS>> testRepo = this.MockedUserRepo();
            UsersLogic ul = new UsersLogic(testRepo.Object);

            // ACT
            USERS user = new USERS()
            {
                USERID = 77,
                LASTNAME = "István",
                FIRSTNAME = "István",
                EMAIL = "istvan@gmail.com",
                ADRESS = "istvan utca 7",
                COUNTRY = "Magyarország",
                PAID = 0,
                PHONE = "06302315477",
                USERNAME = "Istvan",
                USERPASSSWORD = "Istvan20"
            };
            int result = ul.Create(user);

            // ASSERT
            Assert.That(result, Is.EqualTo(0));
        }

        [Test]
        public void LoggedInUserTest()
        {
            // ARRANGE
            Mock<IRepository<USERS>> testRepo = this.MockedUserRepo();
            UsersLogic ul = new UsersLogic(testRepo.Object);

            // ACT
            int result = -1;
            USERS user = ul.LoggIn("probaP", "proba");
            if (user != null)
            {
                result = 0;
            }

            // ASSERT
            Assert.That(result, Is.EqualTo(0));
        }

        [Test]
        public void WrongUserTest()
        {
            // ARRANGE
            Mock<IRepository<USERS>> testRepo = this.MockedUserRepo();
            UsersLogic ul = new UsersLogic(testRepo.Object);

            // ACT
            int result = -1;
            USERS user = ul.LoggIn("ABC", "ABC");
            if (user == null)
            {
                result = 0;
            }

            // ASSERT
            Assert.That(result, Is.EqualTo(0));
        }

        [Test]
        public void UpdateUserTest()
        {
            // ARRANGE
            Mock<IRepository<USERS>> testRepo = this.MockedUserRepo();
            UsersLogic ul = new UsersLogic(testRepo.Object);

            // ACT
            USERS user = testRepo.Object.GetAll().ToList().First();
            user.COUNTRY = "Málta";
            int result = ul.Update(user);

            // ASSERT
            Assert.That(result, Is.EqualTo(0));
        }

        [Test]
        public void DeleteUserTest()
        {
            // ARRANGE
            Mock<IRepository<USERS>> testRepo = this.MockedUserRepo();
            UsersLogic ul = new UsersLogic(testRepo.Object);

            // ACT
            int result = ul.Delete(ul.GetAll().ElementAt(0));

            // ASSERT
            Assert.That(result, Is.EqualTo(0));
        }

        [Test]
        public void AddUserTest_Fail()
        {
            // ARRANGE
            Mock<IRepository<USERS>> testRepo = this.MockedUserRepo();
            UsersLogic ul = new UsersLogic(testRepo.Object);

            // ACT
            USERS user = new USERS()
            {
                USERID = 77,
                LASTNAME = "István",
                FIRSTNAME = "István",
                EMAIL = "istvan@gmail.com",
                ADRESS = "istvan utca 7",
                COUNTRY = "Magyarország",
                PAID = 0,
                PHONE = "06302315477",
                USERNAME = "Istvan",
                USERPASSSWORD = "Istvan20"
            };
            int result = ul.Create(user);

            // ASSERT
            Assert.That(result, Is.Not.EqualTo(1));
        }

        [Test]
        public void DeleteUserTest_Fail()
        {
            // ARRANGE
            Mock<IRepository<USERS>> testRepo = this.MockedUserRepo();
            UsersLogic ul = new UsersLogic(testRepo.Object);

            // ACT
            USERS user = new USERS()
            {
                USERID = 77,
                LASTNAME = "István",
                FIRSTNAME = "István",
                EMAIL = "istvan@gmail.com",
                ADRESS = "istvan utca 7",
                COUNTRY = "Magyarország",
                PAID = 0,
                PHONE = "06302315477",
                USERNAME = "Istvan",
                USERPASSSWORD = "Istvan20"
            };
            int result = ul.Create(user);

            // ASSERT
            Assert.That(result, Is.Not.EqualTo(1));
        }

        [Test]
        public void AddItemTest()
        {
            // ARRANGE
            Mock<IRepository<ITEMS>> testRepo = this.MockedItemsRepo();
            ItemLogic il = new ItemLogic(testRepo.Object);

            // ACT
            ITEMS item = new ITEMS()
            {
                ITEMID = 60,
                ITEMNAME = "probaItem8",
                ITEMTYPE = "CPU",
                ITEMDESCRIPTION = "proba CPU",
                BRAND = "Intel",
                PARAMETER = "4 magos",
                PRICE = 30000
            };

            int result = il.Create(item);

            // ASSERT
            Assert.That(result, Is.EqualTo(0));
        }

        [Test]
        public void AddItemTest_Fail()
        {
            // ARRANGE
            Mock<IRepository<ITEMS>> testRepo = this.MockedItemsRepo();
            ItemLogic il = new ItemLogic(testRepo.Object);

            // ACT
            ITEMS item = new ITEMS()
            {
                ITEMID = 60,
                ITEMNAME = "probaItem8",
                ITEMTYPE = "CPU",
                ITEMDESCRIPTION = "proba CPU",
                BRAND = "Intel",
                PARAMETER = "4 magos",
                PRICE = 30000
            };

            int result = il.Create(item);

            // ASSERT
            Assert.That(result, Is.Not.EqualTo(1));
        }

        [Test]
        public void DeleteItemTest()
        {
            // ARRANGE
            Mock<IRepository<ITEMS>> testRepo = this.MockedItemsRepo();
            ItemLogic il = new ItemLogic(testRepo.Object);

            // ACT
            int result = il.Delete(il.GetAll().ElementAt(0));

            // ASSERT
            Assert.That(result, Is.EqualTo(0));
        }

        [Test]
        public void UpdateItemTest()
        {
            // ARRANGE
            Mock<IRepository<ITEMS>> testRepo = this.MockedItemsRepo();
            ItemLogic il = new ItemLogic(testRepo.Object);
            il.CPUSearch(testRepo.Object.GetAll().ToList(), "CPU");
            // ACT 
            ITEMS item = testRepo.Object.GetAll().ToList().First();
            item.ITEMTYPE = "GPU";
            int result = il.Update(item);

            // ASSERT
            Assert.That(result, Is.EqualTo(0));
        }

        [Test]
        public void CPUItemSearchTest()
        {
            // ARRANGE
            Mock<IRepository<ITEMS>> testRepo = this.MockedItemsRepo();
            ItemLogic il = new ItemLogic(testRepo.Object);

            // ACT 
            int result = -1;
            List<ITEMS> items = il.CPUSearch(testRepo.Object.GetAll().ToList(), "CPU");
            foreach (var item in items)
            {
                if (item.ITEMTYPE == "CPU")
                {
                    result = 0;
                }
                else
                {
                    result = 1;
                }
            }

            // ASSERT
            Assert.That(result, Is.EqualTo(0));
        }

        [Test]
        public void CPUItemSearchTest_Fail()
        {
            // ARRANGE
            Mock<IRepository<ITEMS>> testRepo = this.MockedItemsRepo();
            ItemLogic il = new ItemLogic(testRepo.Object);

            // ACT 
            int result = -1;
            List<ITEMS> items = il.CPUSearch(testRepo.Object.GetAll().ToList(), "CPU");
            foreach (var item in items)
            {
                if (item.ITEMTYPE == "CPU")
                {
                    result = 0;
                }
                else
                {
                    result = 1;
                }
            }

            // ASSERT
            Assert.That(result, Is.Not.EqualTo(1));
        }

        [Test]
        public void SearchByCategorieTest()
        {
            // ARRANGE
            Mock<IRepository<ITEMS>> testRepo = this.MockedItemsRepo();
            ItemLogic il = new ItemLogic(testRepo.Object);

            // ACT
            int result = -1;
            List<ITEMS> items = il.SearchByCategorie(il.GetAll().ToList(), "SSD");
            foreach (var item in items)
            {
                if (item.ITEMDESCRIPTION.Contains("SSD"))
                {
                    result = 0;
                }
                else
                {
                    result = 1;
                }
            }

            // ASSERT
            Assert.That(result, Is.EqualTo(0));
        }

        [Test]
        public void DeletItemTest_Fail()
        {
            // ARRANGE
            Mock<IRepository<ITEMS>> testRepo = this.MockedItemsRepo();
            ItemLogic il = new ItemLogic(testRepo.Object);

            // ACT
            ITEMS item = new ITEMS()
            {
                ITEMID = 60,
                ITEMNAME = "probaItem8",
                ITEMTYPE = "CPU",
                ITEMDESCRIPTION = "proba CPU",
                BRAND = "Intel",
                PARAMETER = "4 magos",
                PRICE = 30000
            };

            int result = il.Delete(item);

            // ASSERT
            Assert.That(result, Is.Not.EqualTo(1));
        }

        [Test]
        public void AddOrderTest()
        {
            // ARRANGE
            Mock<IRepository<ORDERS>> testRepo = this.MockedOrdersRepo();
            OrderLogic ol = new OrderLogic(testRepo.Object);

            // ACT
            ORDERS order = new ORDERS()
            {
                ItemID = 19,
                NumberOfItem = 4,
                OrderDate = DateTime.Now,
                UserID = 7,
                OrderID = 6
            };

            int result = ol.Create(order);

            // ASSERT
            Assert.That(result, Is.EqualTo(0));
        }

        [Test]
        public void AddOrderTest_Fail()
        {
            // ARRANGE
            Mock<IRepository<ORDERS>> testRepo = this.MockedOrdersRepo();
            OrderLogic ol = new OrderLogic(testRepo.Object);

            // ACT
            ORDERS order = new ORDERS()
            {
                ItemID = 20,
                NumberOfItem = 4,
                OrderDate = DateTime.Now,
                UserID = 1,
                OrderID = 9
            };

            int result = ol.Create(order);

            // ASSERT
            Assert.That(result, Is.Not.EqualTo(1));
        }

        [Test]
        public void DeleteOrderTest()
        {
            // ARRANGE
            Mock<IRepository<ORDERS>> testRepo = this.MockedOrdersRepo();
            OrderLogic ol = new OrderLogic(testRepo.Object);

            // ACT
            int result = ol.Delete(ol.GetAll().ElementAt(0));

            // ASSERT
            Assert.That(result, Is.EqualTo(0));
        }

        [Test]
        public void LoggedInAdminTest()
        {
            // ARRANGE
            Mock<IRepository<ADMINS>> testRepo = this.MockedAdminRepo();
            AdminLogic al = new AdminLogic(testRepo.Object);

            // ACT
            int result = -1;
            ADMINS admin = al.LoggIn("Admin1", "1111");
            if (admin != null)
            {
                result = 0;
            }

            // ASSERT
            Assert.That(result, Is.EqualTo(0));
        }

        [Test]
        public void WrongAdminTest()
        {
            // ARRANGE
            Mock<IRepository<ADMINS>> testRepo = this.MockedAdminRepo();
            AdminLogic al = new AdminLogic(testRepo.Object);

            // ACT
            int result = -1;
            ADMINS admin = al.LoggIn("Admin12", "11112");
            if (admin == null)
            {
                result = 0;
            }

            // ASSERT
            Assert.That(result, Is.EqualTo(0));
        }

        private Mock<IRepository<ADMINS>> MockedAdminRepo()
        {
            Mock<IRepository<ADMINS>> testRepo = new Mock<IRepository<ADMINS>>();

            List<ADMINS> testAdmins = new List<ADMINS>();
            testAdmins.Add(new ADMINS()
            {
                ADMINID=4,
                ADMINPASSWORD ="1111",
                USERNAME = "Admin1"
            });
            testAdmins.Add(new ADMINS()
            {
                ADMINID = 5,
                ADMINPASSWORD = "2222",
                USERNAME = "Admin2"
            });
            testRepo.Setup(x => x.GetAll()).Returns(testAdmins.AsQueryable());
            return testRepo;
        }

        private Mock<IRepository<USERS>> MockedUserRepo()
        {
            Mock<IRepository<USERS>> testRepo = new Mock<IRepository<USERS>>();

            List<USERS> testUsers = new List<USERS>();
            testUsers.Add(new USERS()
            {
               USERID = 55,
               LASTNAME = "Próba",
               FIRSTNAME = "Péter",
               EMAIL = "probapeter@gmail.com",
               ADRESS = "proba utca 9",
               COUNTRY = "Magyarország",
               PAID = 0,
               PHONE = "06202315477",
               USERNAME = "probaP",
               USERPASSSWORD = "proba"
            });
            testUsers.Add(new USERS()
            {
                USERID = 52,
                LASTNAME = "Géza",
                FIRSTNAME = "István",
                EMAIL = "gezaistvan@gmail.com",
                ADRESS = "geza utca 91",
                COUNTRY = "Magyarország",
                PAID = 0,
                PHONE = "06202555477",
                USERNAME = "Geza91",
                USERPASSSWORD = "geza91"
            });
            testRepo.Setup(x => x.GetAll()).Returns(testUsers.AsQueryable());
            return testRepo;
        }

        private Mock<IRepository<ITEMS>> MockedItemsRepo()
        {
            Mock<IRepository<ITEMS>> testItemRepo = new Mock<IRepository<ITEMS>>();

            List<ITEMS> testItems = new List<ITEMS>();
            testItems.Add(new ITEMS()
            {
                ITEMID = 55,
                ITEMNAME = "probaItem",
                ITEMTYPE = "CPU",
                ITEMDESCRIPTION = "proba CPU",
                BRAND = "Intel",
                PARAMETER = "6 magos",
                PRICE = 50000
            });
            testItems.Add(new ITEMS()
            {
                ITEMID = 59,
                ITEMNAME = "probaItem2",
                ITEMTYPE = "HDD",
                ITEMDESCRIPTION = "proba HDD",
                BRAND = "AMD",
                PARAMETER = "500 GB",
                PRICE = 15000
            });
            testItems.Add(new ITEMS()
            {
                ITEMID = 45,
                ITEMNAME = "probaItem3",
                ITEMTYPE = "GPU",
                ITEMDESCRIPTION = "proba GPU",
                BRAND = "AMD",
                PARAMETER = "4 GB",
                PRICE = 60000
            });
            testItems.Add(new ITEMS()
            {
                ITEMID = 35,
                ITEMNAME = "probaItem4",
                ITEMTYPE = "SSD",
                ITEMDESCRIPTION = "proba SSD",
                BRAND = "XYZ",
                PARAMETER = "128 GB",
                PRICE = 35000
            });

            testItemRepo.Setup(x => x.GetAll()).Returns(testItems.AsQueryable());

            return testItemRepo;
        }

        private Mock<IRepository<ORDERS>> MockedOrdersRepo()
        {
            Mock<IRepository<ORDERS>> testOrderRepo = new Mock<IRepository<ORDERS>>();

            List<ORDERS> testOrders = new List<ORDERS>();
            testOrders.Add(new ORDERS()
            {
                OrderID = 15,
                OrderDate = DateTime.Now,
                NumberOfItem = 3,
                ItemID = 14,
                UserID = 8
            });
            testOrders.Add(new ORDERS()
            {
                OrderID = 21,
                OrderDate = DateTime.Now,
                NumberOfItem = 2,
                ItemID = 10,
                UserID = 6
            });

            testOrderRepo.Setup(x => x.GetAll()).Returns(testOrders.AsQueryable());
            return testOrderRepo;
        }
    }
}
