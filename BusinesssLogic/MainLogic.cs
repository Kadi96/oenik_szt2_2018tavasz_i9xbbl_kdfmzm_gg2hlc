﻿// <copyright file="MainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BusinesssLogic
{
    using DataBaseLibrary;
    using DataBaseLibrary.Interfaces;
    using DataBaseLibrary.Repositories;
    using System;
    using System.IO;
    using System.Reflection;

    /// <summary>
    /// MainLogic Class
    /// </summary>
    public class MainLogic
    {
        private static MainLogic ml;

        private DatabaseEntities entites;
        private ItemLogic itemLogic;
        private AdminLogic adminLogic;
        private UsersLogic userLogic;
        private OrderLogic orderLogic;

        private IRepository<ORDERS> orderRepo;
        private IRepository<ITEMS> itemRepo;
        private IRepository<USERS> userRepo;
        private IRepository<ADMINS> adminRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainLogic"/> class.
        /// Constructor of OrderLogic class.
        /// </summary>
        public MainLogic()
        {
            this.entites = new DatabaseEntities();
            this.OrderRepo = new OrderRepository(this.entites);
            this.adminRepo = new AdminRepository(this.entites);
            this.itemRepo = new ItemRepository(this.entites);
            this.userRepo = new UserRepository(this.entites);
        }

        /// <summary>
        /// Gets the MainLogic.
        /// </summary>
        /// <value>Get MainLogic.</value>
        public static MainLogic Get
        {
            get
            {
                if (Ml == null)
                {
                    Ml = new MainLogic();
                }

                return Ml;
            }
        }

        /// <summary>
        /// Gets or set the MainLogic.
        /// </summary>
        /// <value>Get MainLogic.</value>
        public static MainLogic Ml { get => ml; set => ml = value; }

        /// <summary>
        /// Gets or sets the ItemLogic.
        /// </summary>
        /// <value>Get ItemLogic.</value>
        public ItemLogic ItemLogic { get => this.itemLogic; set => this.itemLogic = value; }

        /// <summary>
        /// Gets or sets the AdminLogic.
        /// </summary>
        /// <value>Get AdminLogic.</value>
        public AdminLogic AdminLogic { get => this.adminLogic; set => this.adminLogic = value; }

        /// <summary>
        /// Gets or sets the UserLogic.
        /// </summary>
        /// <value>Get UserLogic.</value>
        public UsersLogic UserLogic { get => this.userLogic; set => this.userLogic = value; }

        /// <summary>
        /// Gets or sets the OrderLogic.
        /// </summary>
        /// <value>Get OrderLogic.</value>
        public OrderLogic OrderLogic { get => this.orderLogic; set => this.orderLogic = value; }

        /// <summary>
        /// Gets or sets the OrderRepo.
        /// </summary>
        /// <value>Get OrderRepo.</value>
        public IRepository<ORDERS> OrderRepo { get => this.orderRepo; set => this.orderRepo = value; }

        /// <summary>
        /// Add UserLogic to UserLogic variable.
        /// </summary>
        public void AddUserLogic()
        {
            this.UserLogic = new UsersLogic(this.userRepo);
        }

        /// <summary>
        /// Add AddAdminLogic to AddAdminLogic variable.
        /// </summary>
        public void AddAdminLogic()
        {
            this.AdminLogic = new AdminLogic(this.adminRepo);
        }

        /// <summary>
        /// Add AddItemLogic to AddItemLogic variable.
        /// </summary>
        public void AddItemLogic()
        {
            this.ItemLogic = new ItemLogic(this.itemRepo);
        }

        /// <summary>
        /// Add AddOrderLogic to AddOrderLogic variable.
        /// </summary>
        public void AddOrderLogic()
        {
            this.OrderLogic = new OrderLogic(this.OrderRepo);
        }

        public void WriteLog(string belepes)
        {
            string path = Path.GetDirectoryName(Path.GetDirectoryName(Path.GetDirectoryName(Path.GetDirectoryName(
            Assembly.GetExecutingAssembly().GetName().CodeBase)))) + "\\belepesek\\belepesek.txt";
            StreamWriter sw = new StreamWriter(new Uri(path).LocalPath);
            sw.WriteLine(belepes);
            sw.Close();
        }
    }
}
