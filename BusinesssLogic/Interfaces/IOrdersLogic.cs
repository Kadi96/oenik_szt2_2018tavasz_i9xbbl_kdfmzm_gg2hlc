﻿// <copyright file="IOrdersLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BusinesssLogic.Interfaces
{
    using System.Collections.Generic;
    using DataBaseLibrary;

    internal interface IOrdersLogic
    {
        int Create(ORDERS order);

        int Update(ORDERS order);

        int Delete(ORDERS order);

        IEnumerable<ORDERS> GetAll();
    }
}
