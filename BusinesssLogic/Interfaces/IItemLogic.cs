﻿// <copyright file="IItemLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BusinesssLogic.Interfaces
{
    using System.Collections.Generic;
    using DataBaseLibrary;

    public interface IItemLogic
    {
        int Create(ITEMS item);

        int Update(ITEMS item);

        int Delete(ITEMS item);

        IEnumerable<ITEMS> GetAll();
    }
}
