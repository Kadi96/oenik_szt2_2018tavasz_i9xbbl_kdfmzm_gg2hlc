﻿// <copyright file="IAdminLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BusinesssLogic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataBaseLibrary;

    internal interface IAdminLogic
    {
        IEnumerable<ADMINS> GetAll();
    }
}
