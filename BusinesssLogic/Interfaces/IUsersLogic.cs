﻿// <copyright file="IUsersLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BusinesssLogic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataBaseLibrary;

    internal interface IUsersLogic
    {
        int Create(USERS user);

        int Update(USERS user);

        int Delete(USERS user);

        IEnumerable<USERS> GetAll();
    }
}
