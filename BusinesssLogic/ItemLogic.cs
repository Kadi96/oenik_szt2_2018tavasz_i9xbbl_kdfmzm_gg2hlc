﻿// <copyright file="ItemLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BusinesssLogic
{
    using System.Collections.Generic;
    using System.Linq;
    using BusinesssLogic.Interfaces;
    using DataBaseLibrary;
    using DataBaseLibrary.Interfaces;

    /// <summary>
    /// ItemLogic Class
    /// </summary>
    public class ItemLogic : IItemLogic
    {
        private IRepository<ITEMS> itemRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemLogic"/> class.
        /// Constructor of ItemLogic class.
        /// </summary>
        /// <param name="itemRepo">IRepository<ITEMS> parameter.</param>
        public ItemLogic(IRepository<ITEMS> itemRepo)
        {
            this.itemRepository = itemRepo;
        }

        /// <summary>
        /// To creat a new item.
        /// </summary>
        /// <param name="item">Creatable new Item.</param>
        /// <returns>int</returns>
        public int Create(ITEMS item)
        {
            int addedItem = -1;
            addedItem = this.itemRepository.Create(item);
            return addedItem;
        }

        /// <summary>
        /// To creat a new item.
        /// </summary>
        /// <param name="item">Creatable new Item.</param>
        /// <returns>int</returns>
        public int Delete(ITEMS item)
        {
            int deletedItem = -1;
            deletedItem = this.itemRepository.Delete(item);
            return deletedItem;
        }

        /// <summary>
        /// To creat a new item.
        /// </summary>
        /// <param name="item">Creatable new Item.</param>
        /// <returns>int</returns>
        public IEnumerable<ITEMS> GetAll()
        {
            return this.itemRepository.GetAll().ToList();
        }

        /// <summary>
        /// To creat a new item.
        /// </summary>
        /// <param name="item">Creatable new Item.</param>
        /// <returns>int</returns>
        public int Update(ITEMS item)
        {
            int updatedItem = -1;
            updatedItem = this.itemRepository.Update(item);
            return updatedItem;
        }

        // Keresések

        /// <summary>
        /// Search in all categorie.
        /// </summary>
        /// <param name="items">item parameter.</param>
        /// <param name="categ">The categorie parameter.</param>
        /// <returns>List<ITEMS></returns>
        public List<ITEMS> SearchByCategorie(List<ITEMS> items, string categ)
        {
            return items.Where(x => x.ITEMTYPE == categ).ToList();
        }

        /// <summary>
        /// Search by keywords.
        /// </summary>
        /// <param name="items">item parameter.</param>
        /// <param name="keyword">keyword parameter.</param>
        /// <returns>List<ITEMS></returns>
        public List<ITEMS> SearchByKeyword(List<ITEMS> items, string keyword)
        {
            return items.Where(x => x.BRAND.Contains(keyword) || x.ITEMDESCRIPTION.Contains(keyword) || x.ITEMNAME.Contains(keyword)).ToList();
        }

        /// <summary>
        /// Search in all CPU.
        /// </summary>
        /// <param name="items">item parameter.</param>
        /// <param name="brand">the brand of the item.</param>
        /// <returns>List<ITEMS></returns>
        public List<ITEMS> CPUSearch(List<ITEMS> items, string brand)
        {
            return items.Where(x => (x.ITEMNAME.Contains(brand) || x.ITEMDESCRIPTION.Contains(brand)) && x.ITEMTYPE == "CPU").ToList();
        }

        /// <summary>
        /// Search in all HDD.
        /// </summary>
        /// <param name="items">item parameter.</param>
        /// <param name="type">the type of the item.</param>
        /// <returns>List<ITEMS></returns>
        public List<ITEMS> HDDSearch(List<ITEMS> items, string type)
        {
            return items.Where(x => (x.ITEMNAME.Contains(type) || x.PARAMETER == type) && x.ITEMTYPE == "HDD").ToList();
        }

        /// <summary>
        /// Search in all SSD.
        /// </summary>
        /// <param name="items">item parameter.</param>
        /// <param name="type">the type of the item.</param>
        /// <returns>List<ITEMS></returns>
        public List<ITEMS> SSDSearch(List<ITEMS> items, string type)
        {
            return items.Where(x => (x.ITEMNAME.Contains(type) || x.PARAMETER == type) && x.ITEMTYPE == "SSD").ToList();
        }

        /// <summary>
        /// Search in all RAM.
        /// </summary>
        /// <param name="items">item parameter.</param>
        /// <param name="type">the type of the item.</param>
        /// <returns>List<ITEMS></returns>
        public List<ITEMS> RAMSearch(List<ITEMS> items, string type)
        {
            return items.Where(x => (x.ITEMNAME.Contains(type) || x.PARAMETER == type) && x.ITEMTYPE == "Memória").ToList();
        }
    }
}
