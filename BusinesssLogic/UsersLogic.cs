﻿// <copyright file="UsersLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BusinesssLogic
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    using BusinesssLogic.Interfaces;
    using DataBaseLibrary;
    using DataBaseLibrary.Interfaces;
 

    /// <summary>
    /// UserLogic Class
    /// </summary>
    public class UsersLogic : IUsersLogic
    {
        private IRepository<USERS> usersRepository;

        
        /// <summary>
        /// To Hash the password.
        /// </summary>
        /// <param name="psw">User's password .</param>
        /// <returns>string</returns>
        public static string Hash(string psw)
        {
            var hash = new SHA1Managed().ComputeHash(Encoding.UTF8.GetBytes(psw));
            return string.Join(string.Empty, hash.Select(x => x.ToString("x2")).ToArray());
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UsersLogic"/> class.
        /// Constructor of UserLogic class.
        /// </summary>
        /// <param name="userRepo">IRepository<USERS> parameter.</param>
        public UsersLogic(IRepository<USERS> userRepo)
        {
            this.usersRepository = userRepo;
        }

        /// <summary>
        /// To creat a new user.
        /// </summary>
        /// <param name="user">Creatable User.</param>
        /// <returns>int</returns>
        public int Create(USERS user)
        {
            int addUser = -1;
            addUser = this.usersRepository.Create(user);
            return addUser;
        }

        /// <summary>
        /// To delete a user.
        /// </summary>
        /// <param name="user">Delete User.</param>
        /// <returns>int</returns>
        public int Delete(USERS user)
        {
            int deleteUser = -1;
            deleteUser = this.usersRepository.Delete(user);
            return deleteUser;
        }

        /// <summary>
        /// To get all users in a list.
        /// </summary>
        /// <returns>IEnumerable<USERS></returns>
        public IEnumerable<USERS> GetAll()
        {
            return this.usersRepository.GetAll().ToList();
        }

        /// <summary>
        /// To update a user.
        /// </summary>
        /// <param name="user">Update User.</param>
        /// <returns>int</returns>
        public int Update(USERS user)
        {
            int updatUser = -1;
            updatUser = this.usersRepository.Update(user);
            return updatUser;
        }

        /// <summary>
        /// To get the logged user.
        /// </summary>
        /// <param name="userName">Logged in username.</param>
        /// <param name="psw">User's password.</param>
        /// <returns>USERS</returns>
        public USERS LoggIn(string userName, string psw)
        {
            string hashPassword = UsersLogic.Hash(psw).ToString();
            var user = this.usersRepository.GetAll().SingleOrDefault(x => x.USERNAME == userName && x.USERPASSSWORD == hashPassword);          
            return user;
        }
    }
}
