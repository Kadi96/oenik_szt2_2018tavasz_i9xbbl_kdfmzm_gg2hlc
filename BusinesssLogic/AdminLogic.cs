﻿// <copyright file="AdminLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BusinesssLogic
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    using BusinesssLogic.Interfaces;
    using DataBaseLibrary;
    using DataBaseLibrary.Interfaces;

    /// <summary>
    /// AdminLogic Class
    /// </summary>
    public class AdminLogic : IAdminLogic
    {
        private IRepository<ADMINS> adminRepository;

        /// <summary>
        /// To Hash the password.
        /// </summary>
        /// <param name="psw">User's password .</param>
        /// <returns>string</returns>
        public static string Hash(string psw)
        {
            var hash = new SHA1Managed().ComputeHash(Encoding.UTF8.GetBytes(psw));
            return string.Join(string.Empty, hash.Select(x => x.ToString("x2")).ToArray());
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AdminLogic"/> class.
        /// Constructor of AdminLogic class.
        /// </summary>
        /// <param name="adminRepo">IRepository<ADMINS> parameter.</param>
        public AdminLogic(IRepository<ADMINS> adminRepo)
        {
            this.adminRepository = adminRepo;
        }

        /// <summary>
        /// To get all Admin.
        /// </summary>
        /// <returns>IEnumerable<ADMINS></returns>
        public IEnumerable<ADMINS> GetAll()
        {
            return this.adminRepository.GetAll().ToList();
        }

        /// <summary>
        /// To get the logged admin.
        /// </summary>
        /// <param name="userName">Logged Admin.</param>
        /// <param name="psw">Admin's password.</param>
        /// <returns>ADMINS</returns>
        public ADMINS LoggIn(string userName, string psw)
        {
            string hashPassword = UsersLogic.Hash(psw).ToString();
            var admin = this.adminRepository.GetAll().SingleOrDefault(x => x.USERNAME == userName && x.ADMINPASSWORD == hashPassword);
            return admin;
        }
    }
}
