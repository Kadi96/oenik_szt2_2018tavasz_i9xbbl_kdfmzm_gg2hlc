﻿// <copyright file="OrderLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BusinesssLogic
{
    using System.Collections.Generic;
    using System.Linq;
    using BusinesssLogic.Interfaces;
    using DataBaseLibrary;
    using DataBaseLibrary.Interfaces;

    /// <summary>
    /// Order Logic Class
    /// </summary>
    public class OrderLogic : IOrdersLogic
    {
        private IRepository<ORDERS> orderRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderLogic"/> class.
        /// Constructor of OrderLogic class.
        /// </summary>
        /// <param name="orderRepo">IRepository<ORDERS> parameter.</param>
        public OrderLogic(IRepository<ORDERS> orderRepo)
        {
            this.orderRepo = orderRepo;
        }

        /// <summary>
        /// To creat a new order.
        /// </summary>
        /// <param name="order">Creatable Order.</param>
        /// <returns>int</returns>
        public int Create(ORDERS order)
        {
            int addOrder = -1;
            addOrder = this.orderRepo.Create(order);
            return addOrder;
        }

        /// <summary>
        /// To delet an order.
        /// </summary>
        /// <param name="order">Delet Order.</param>
        /// <returns>int</returns>
        public int Delete(ORDERS order)
        {
            int deletOrder = -1;
            deletOrder = this.orderRepo.Delete(order);
            return deletOrder;
        }

        /// <summary>
        /// To get all Orders.
        /// </summary>
        /// <returns>IEnumerable<ORDERS></returns>
        public IEnumerable<ORDERS> GetAll()
        {
            return this.orderRepo.GetAll().ToList();
        }

        /// <summary>
        /// To ipdate an order.
        /// </summary>
        /// <param name="order">Update Order.</param>
        /// <returns>int</returns>
        public int Update(ORDERS order)
        {
            int updateOrder = -1;
            updateOrder = this.orderRepo.Update(order);
            return updateOrder;
        }
    }
}
