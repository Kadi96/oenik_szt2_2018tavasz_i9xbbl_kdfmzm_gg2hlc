﻿// <copyright file="WebRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DataBaseLibrary
{
    using System.Linq;

    public class WebRepo : IWebRepo
    {
        private DatabaseEntities ed;

        public WebRepo()
        {
            this.ed = new DatabaseEntities();
        }

        public IQueryable<ITEMS> GetAllDItems()
        {
            return this.ed.ITEMS;
        }

        public IQueryable<ADMINS> GetAllEAdmins()
        {
            return this.ed.ADMINS;
        }

        public IQueryable<ORDERS> GetAllOrders()
        {
            return this.ed.ORDERS;
        }

        public IQueryable<USERS> GetAllUsers()
        {
            return this.ed.USERS;
        }
    }
}
