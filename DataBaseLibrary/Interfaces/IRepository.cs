﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DataBaseLibrary.Interfaces
{
    using System.Linq;

    /// <summary>
    /// Main Repository interface.
    /// </summary>
    public interface IRepository<T>
    {
        int Create(T newItem);

        int Update(T item);

        int Delete(T item);

        IQueryable<T> GetAll();
    }
}
