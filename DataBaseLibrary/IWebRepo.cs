﻿// <copyright file="IWebRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DataBaseLibrary
{
    using System.Linq;

    internal interface IWebRepo
    {
        IQueryable<ADMINS> GetAllEAdmins();

        IQueryable<ITEMS> GetAllDItems();

        IQueryable<ORDERS> GetAllOrders();

        IQueryable<USERS> GetAllUsers();
    }
}
