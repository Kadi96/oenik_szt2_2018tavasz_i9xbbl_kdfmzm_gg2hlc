﻿// <copyright file="ItemRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DataBaseLibrary.Repositories
{
    public class ItemRepository : GenericRepository<ITEMS>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ItemRepository"/> class.
        /// Item Repository.
        /// </summary>
        /// <param name="dataBaseEntites">DateBase parameter.</param>
        public ItemRepository(DatabaseEntities dataBaseEntities)
            : base(dataBaseEntities)
        {
        }
    }
}
