﻿// <copyright file="GenericRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DataBaseLibrary.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Validation;
    using System.Diagnostics;
    using System.Linq;
    using DataBaseLibrary.Interfaces;

    /// <summary>
    /// Initializes a new instance of the <see cref="GenericRepository"/> class.
    /// The Main Generci Repository.
    /// </summary>
    public abstract class GenericRepository<T> : IRepository<T>
        where T : class
    {
        private DatabaseEntities dataBaseEntities;

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericRepository{T}"/> class.
        /// Constructor of GenericRepository class.
        /// </summary>
        /// <param name="dBentities">Database parameter.</param>
        public GenericRepository(DatabaseEntities dBentities)
        {
            this.dataBaseEntities = dBentities;
        }

        /// <summary>
        /// The main Create method.
        /// </summary>
        /// <param name="newItem">Create a new.</param>
        /// <returns>int</returns>
        public virtual int Create(T newItem)
        {
            try
            {
                if (newItem == null)
                {
                    throw new ArgumentException();
                }
                else
                {
                    this.dataBaseEntities.Set<T>().Add(newItem);
                    return this.dataBaseEntities.SaveChanges();
                }
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}",
                                                validationError.PropertyName,
                                                validationError.ErrorMessage);
                    }
                }
                return 0;
            }
        }

        /// <summary>
        /// The main Delete method.
        /// </summary>
        /// <param name="item">Delete an item.</param>
        /// <returns>int</returns>
        public int Delete(T item)
        {
            if (item == null)
            {
                throw new ArgumentException();
            }
            else
            {
                this.dataBaseEntities.Set<T>().Remove(item);
                return this.dataBaseEntities.SaveChanges();
            }
        }

        /// <summary>
        /// The main GetAll method.
        /// </summary>
        /// <returns>IQueryable<T></returns>
        public IQueryable<T> GetAll()
        {
            return this.dataBaseEntities.Set<T>().AsQueryable();
        }

        /// <summary>
        /// The main Update method.
        /// </summary>
        /// <param name="item">Updateed item.</param>
        /// <returns>int</returns>
        public int Update(T item)
        {
            if (item == null)
            {
                throw new ArgumentException();
            }
            else
            {
                this.dataBaseEntities.Entry<T>(item).State = EntityState.Modified;
                return this.dataBaseEntities.SaveChanges();
            }
        }
    }
}
