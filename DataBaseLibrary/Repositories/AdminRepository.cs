﻿// <copyright file="AdminRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DataBaseLibrary.Repositories
{
    public class AdminRepository : GenericRepository<ADMINS>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AdminRepository"/> class.
        /// Admin Repository.
        /// </summary>
        /// <param name="dataBaseEntites">DateBase parameter.</param>
        public AdminRepository(DatabaseEntities dataBaseEntites)
            : base(dataBaseEntites)
        {
        }
    }
}
