﻿// <copyright file="OrderRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DataBaseLibrary.Repositories
{
    public class OrderRepository : GenericRepository<ORDERS>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OrderRepository"/> class.
        /// Order Repository.
        /// </summary>
        /// <param name="dataBaseEntites">DateBase parameter.</param>
        public OrderRepository(DatabaseEntities dataBaseEntities)
            : base(dataBaseEntities)
        {
        }
    }
}
