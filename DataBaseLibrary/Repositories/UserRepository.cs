﻿// <copyright file="UserRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DataBaseLibrary.Repositories
{
    public class UserRepository : GenericRepository<USERS>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserRepository"/> class.
        /// User Repository.
        /// </summary>
        /// <param name="dataBaseEntites">DateBase parameter.</param>
        public UserRepository(DatabaseEntities dataBaseEntities)
            : base(dataBaseEntities)
        {
        }
    }
}
