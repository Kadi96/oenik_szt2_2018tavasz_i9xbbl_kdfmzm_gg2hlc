//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataBaseLibrary
{
    using System;
    using System.Collections.Generic;
    
    public partial class ORDERS
    {
        public int OrderID { get; set; }
        public int ItemID { get; set; }
        public int UserID { get; set; }
        public int NumberOfItem { get; set; }
        public System.DateTime OrderDate { get; set; }
    
        public virtual ITEMS ITEMS { get; set; }
        public virtual USERS USERS { get; set; }
    }
}
